import React , { Component } from 'react';
import Signup from "./Signup";
import { BrowserRouter as Router } from 'react-router-dom';
import {Route, history, Switch, Link} from 'react-router-dom';

class Login extends Component{

    render(){
return(
    
        <div className="content-wrapper">
        <h1>Log in</h1>
        <p>Don't have an account <Link to="/Signup">Signup</Link></p> 
         <form>
         <input type="email" placeholder="Username"/>
         <input type="password" placeholder="Password"/>
         <label for="rem">Remember</label>
         <input type="checkbox" id="rem"/>
         <input className="btn1" type="button" value="Login"/>
         </form>
         <input id="btn2" type="button" value="Cancel"/>
        </div>
    );
    }
}
export default Login;