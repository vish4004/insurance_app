import React from 'react';
import ReactDOM from 'react-dom';
import "./style.css";
import Signup from "./Signup";
import Login from './Login';
import { BrowserRouter as Router } from 'react-router-dom';
import {Route, history, Switch, Link} from 'react-router-dom';

var target = document.querySelector('#main-container');
ReactDOM.render(
    
    <Router history>
      <div>
        <Route path="/" exact component={Login} />
        <Route path="/Signup" exact component={Signup} />
      </div>
    </Router>,
    target
  );
    